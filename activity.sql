

SELECT customerName 
FROM customers 
WHERE country = "Philippines";


SELECT contactLastName, contactFirstname
FROM customers
WHERE customerName = "La Rochelle Gifts";

SELECT productName , MSRP
FROM products
WHERE productName = "The Titanic";

SELECT lastName, firstName
FROM employees
WHERE email = "jfirrelli@classicmodelcars.com";

SELECT customerName
FROM customers
WHERE state IS NULL;

SELECT firstName, lastName, email
FROM employees
WHERE lastName = "Patterson"
AND firstName = "Steve";

SELECT customerName, country, creditLimit
FROM customers
WHERE country != "USA"
AND creditLimit > 3000;

SELECT customerName
FROM customers
WHERE customerName NOT LIKE "%A%";

SELECT customerNumber
FROM orders
WHERE comments LIKE '%DHL%';

SELECT productLine
FROM productlines
WHERE textDescription LIKE "%state of the art%";

SELECT DISTINCT country 
FROM customers;

SELECT DISTINCT status
FROM orders;

SELECT customerName, country
FROM customers
WHERE country = "USA"
OR country ="France"
OR country = "Canada";


SELECT employees.lastName , employees.firstName, offices.city
FROM employees JOIN offices
ON employees.officeCode = offices.officeCode
WHERE offices.city = "Tokyo";


SELECT customerName
FROM customers JOIN employees
ON customers.salesRepEmployeeNumber = employees.employeeNumber
WHERE employees.lastName = "Thompson"
AND employees.firstName = "Leslie";


SELECT employees.firstName, employees.lastName, customers.customerName, offices.country
FROM employees JOIN customers
ON customers.salesRepEmployeeNumber = employees.employeeNumber
JOIN offices
ON customers.country = offices.country;

SELECT lastName, firstName
FROM employees
WHERE reportsTo = (SELECT employeeNumber FROM employees WHERE firstName = "Anthony" AND lastName = "Bow");

SELECT productName, MSRP
FROM products
WHERE MSRP = (SELECT MAX(MSRP) FROM products);

SELECT COUNT("UK")
FROM customers;

SELECT 
    productLine, 
    COUNT(*)
FROM
    products
GROUP BY productLine;


SELECT 
    salesRepEmployeeNumber, 
    COUNT(*)
FROM
    customers
GROUP BY salesRepEmployeeNumber;

SELECT productName, quantityInStock
FROM products
WHERE productLine = "Planes"
AND quantityInStock < 1000;

